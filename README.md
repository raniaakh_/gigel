# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

1. Make sure you have installed ruby '2.5.3', rails '5.2.1', mysql, insomnia

2. clone repository by typing:
```bash
$   git clone https://bitbucket.org/raniaakh_/gigel.git
```

3. After cloning success, bundle the apps by typing:
```bash
$   bundle install
```

4. Before you create database you need to update username and password mysql configuration on file ' database.yml '. Create database and its table by typing. :
```bash
$   rails db:create
$   rails db:migrate
```

5. Run rails server by typing:
```bash
$   rails s
```

6. Import file ' gigel_2019-01-09.json ' in insomnia apps to run API. There are 4 endpoints which able to run, You need to run register endpoint first.