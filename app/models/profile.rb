class Profile < ApplicationRecord
    has_secure_password

    validates :username, presence: true, uniqueness: true, length: { :in => 3..20 }
    validates :email, presence: true, uniqueness: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
    validates :password, presence: true, confirmation: true
    validates :full_name, presence: true
    validates :phone_number, presence: true, uniqueness: true, length: { :in => 9..15 }
    validate :password_complexity
  
    def password_complexity
        return if password.blank? || password =~ /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,20}$/

        errors.add :password, 'Password Length should be 8-20 characters and include: 1 uppercase, 1 lowercase, 1 digit and 1 special character'
    end
end
