class AuthenticationController < ApplicationController
    skip_before_action :verify_authenticity_token  

    def register
        Profile.transaction do
            user = Profile.new(user_params)
            user.save!
            render json: user, status: :ok
        end
    end

    def login
        if auth_user
            response = {
                    full_name: auth_user.full_name,
                    username: auth_user.username,
                    email: auth_user.email,
                    updated_at: auth_user.updated_at
                }
            render json: response, status: :ok
        else
            render json: {message: "user has no authorization"}, status: :ok
        end
    end

    def update_profile
        user = Profile.find_by(username: params[:username])

        if user.present?
            if auth_user
                Profile.transaction do
                    user.update!(user_params)
                    response = {
                        full_name: user.full_name,
                        username: user.username,
                        email: user.email,
                        phone_number: user.phone_number,
                        updated_at: user.updated_at
                    }
                    render json: response, status: :ok
                end
            else
                render json: {message: "user has no authorization"}, status: :ok
            end
        else
            render json: {message: "user not found"}, status: :ok
        end
    end

    def update_password
        if auth_user
            if params[:new_password] == params[:confirm_password]
                Profile.transaction do
                    auth_user.update!(password: params[:new_password])
                    render json: {message: "password has been changed"}, status: :ok
                end
            else
                render json: {message: "password confirmation is not match"}, status: :ok
            end
        else
            render json: {message: "user has no authorization"}, status: :ok
        end
    end

    private

    def auth_user
        user = Profile.find_by(email: user_params[:email])

        if user and user.authenticate(user_params[:password])
            return user
        end
    end

    def user_params
        params.require(:authentication).permit(:full_name, :username, :email, :password, :phone_number)
    end
end