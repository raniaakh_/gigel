Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    post 'register', to: 'authentication#register' 
    post 'login', to: 'authentication#login' 
    put 'update_password', to: 'authentication#update_password' 
    put 'update_profile/:username', to: 'authentication#update_profile'
end
